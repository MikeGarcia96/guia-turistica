import { Component, ViewChild } from '@angular/core';
import { Nav, Platform, AlertController } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { HomePage } from '../pages/home/home';
import { CategoriaPage } from '../pages/categoria/categoria';
import { ModelProvider } from '../providers/model/model';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  rootPage: any = HomePage;

  pages: Array<{title: string, component: any, icon: any}>;

  constructor(
    public platform: Platform, 
    public statusBar: StatusBar, 
    public splashScreen: SplashScreen,
    public db: ModelProvider,
    public alertCtrl: AlertController) {
    this.initializeApp();
    

  }

  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.styleLightContent();
      this.statusBar.backgroundColorByHexString('#B03648');
      this.splashScreen.hide();
    });
  }

  openPage(page, category, name) {
    let loading;
    loading = this.db.loadNotification(this.db.cargando);
    loading.present();
    let options = {
      categoria: category,
      loading: loading,
      name: name,
    }
    this.nav.setRoot(page,options);
  }

}
