import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule, IonicPageModule } from 'ionic-angular';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { MyApp } from './app.component';
import { HomePage } from "../pages/home/home";
import { CategoriaPage } from "../pages/categoria/categoria";
import { HomePageModule } from '../pages/home/home.module';
import { CategoriaPageModule } from '../pages/categoria/categoria.module';

import { HttpClientModule } from '../../node_modules/@angular/common/http';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { ModelProvider } from '../providers/model/model';

// Navigator
import { LaunchNavigator } from '@ionic-native/launch-navigator';
import { StarRatingModule } from 'ionic3-star-rating';

// Import ionic2-rating module
import { Ionic2RatingModule } from 'ionic2-rating';


@NgModule({
  declarations: [
    MyApp,
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    StarRatingModule,
    IonicPageModule.forChild(MyApp),
    Ionic2RatingModule, // Put ionic2-rating module here
    HomePageModule,
    CategoriaPageModule,
    HttpClientModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    CategoriaPage,
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    ModelProvider,
    LaunchNavigator,
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AppModule {}
