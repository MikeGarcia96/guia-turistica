import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Platform, App } from 'ionic-angular';
import { LaunchNavigator, LaunchNavigatorOptions } from '@ionic-native/launch-navigator';
import { ModelProvider } from '../../providers/model/model';
import { SafeUrl, DomSanitizer } from '@angular/platform-browser';

/**
 * Generated class for the ContenidoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-contenido',
  templateUrl: 'contenido.html',
})
export class ContenidoPage {

  private sitio : any;
  private galeria: any[];

  constructor(
    public navCtrl: NavController,
    private launchNavigator: LaunchNavigator, 
    public navParams: NavParams,
    public db: ModelProvider,
    private sanitizer: DomSanitizer,
    public platform: Platform,
    public app: App,) {
      this.sitio = this.navParams;
      console.log(this.sitio);
      // Galeria
      this.galeria=this.sitio.data.galeria.split('|');
      console.log(this.galeria);

      //Back button hardware
      this.platform.registerBackButtonAction(() => {
        this.navCtrl.pop();
      });
  }

  openPlace(latitud: string, longitud:string){
    let options: LaunchNavigatorOptions = {
      app: this.launchNavigator.APP.GOOGLE_MAPS
    };
    this.launchNavigator.navigate(latitud + ',' + longitud, options);
  }

  getImgContent(img): SafeUrl {
    return this.sanitizer.bypassSecurityTrustUrl(img);
  }

}
