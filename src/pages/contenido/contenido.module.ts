import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ContenidoPage } from './contenido';
import { Ionic2RatingModule } from "ionic2-rating";

@NgModule({
  declarations: [
    ContenidoPage,
  ],
  imports: [
    IonicPageModule.forChild(ContenidoPage),
    Ionic2RatingModule // Put ionic2-rating module here
  ],
})
export class ContenidoPageModule {}
