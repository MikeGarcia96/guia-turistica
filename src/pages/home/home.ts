import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, Platform, App } from 'ionic-angular';
import { ModelProvider } from "../../providers/model/model";
import { CategoriaPage } from "../categoria/categoria";


@IonicPage()
@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  sitios: any;
  public loading: any;
  public params: any;
  private alert;

  constructor(
    public navCtrl: NavController,
    public db: ModelProvider,
    public navParams: NavParams,
    public alertCtrl: AlertController,
    public platform: Platform,
    public app: App,
  ) {

    this.params = this.navParams;
    if(this.params.data.loading == null){
      this.loadingInicial();
    }

    //Back button hardware
    this.platform.registerBackButtonAction(() => {
      // Catches the active view
      let nav = this.app.getActiveNavs()[0];
      let activeView = nav.getActive();                
      // Checks if can go back before show up the alert
      if(activeView.name === 'HomePage') {
        if(this.alert){
          this.alert.dismiss();
          this.alert = false;
        }else{
          this.alert = this.alertCtrl.create({
            title: 'Guia Turistica',
            message: this.db.deseaSalir,
            buttons: [{
                text: this.db.cancelar,
                role: 'cancel',
                handler: () => {
                }
            },{
                text: this.db.aceptar,
                handler: () => {
                  this.platform.exitApp();
                }
            }]
          });
          this.alert.present();
        }
        console.log(this.alert);
      }else{
        this.navCtrl.pop();
      }
    });

  }

  loadingInicial(){
    this.loading = this.db.loadNotification(this.db.cargando);
    this.loading.present();
  }

  ionViewDidLoad() {
    if(this.params.data.loading == null){
      this.loading.dismiss();
    }else{
      this.params.data.loading.dismiss();
    }
  }

  openCategory(category : string, name : string){
    let loading;
    loading = this.db.loadNotification(this.db.cargando);
    loading.present();
    let options = {
      categoria: category,
      loading: loading,
      name: name,
    }
    this.navCtrl.setRoot(CategoriaPage, options);
  }

  


}
