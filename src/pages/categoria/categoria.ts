import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController, Platform, App, AlertController } from 'ionic-angular';
import { ModelProvider } from '../../providers/model/model';
import { LaunchNavigator, LaunchNavigatorOptions } from '@ionic-native/launch-navigator';
import { HomePage } from '../home/home';
import { DomSanitizer, SafeUrl } from '@angular/platform-browser';

/**
 * Generated class for the CategoriaPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-categoria',
  templateUrl: 'categoria.html',
})
export class CategoriaPage {

  params: any;
  sitios: any = [];
  loading: any;
  private alert;
  sitiosAux: any = [];
  public arraySites: any[] = [];
  public sitiosArray: any[] = [];

  constructor(
    public navCtrl: NavController,
    public db: ModelProvider,
    private launchNavigator: LaunchNavigator, 
    public navParams: NavParams,
    private toastCtrl: ToastController,
    private sanitizer: DomSanitizer,
    public platform: Platform,
    public app: App,
    public alertCtrl: AlertController,

  ) {
    this.params = this.navParams;
    //Back button hardware
    this.platform.registerBackButtonAction(() => {
      this.navCtrl.setRoot('HomePage');
    });

  }

  openPlace(latitud: string, longitud:string){
    let options: LaunchNavigatorOptions = {
      app: this.launchNavigator.APP.GOOGLE_MAPS
    };
    this.launchNavigator.navigate(latitud + ',' + longitud, options);
  }

  openDetailPlace(sitio: any){
    this.navCtrl.push('ContenidoPage', sitio);
  }

  ionViewDidLoad(){
    this.db.obtenSitios().subscribe(
      (data) => {
        this.params.data.loading.dismiss();
        this.params.data.categoria
        let sites = JSON.parse( JSON.stringify( data['sitios'] ) );
        
        this.arraySites = Object.keys(sites).map(key => sites[key]);
        for (let index = 0; index < this.arraySites.length; index++) {
          if(this.arraySites[index]['categoria'] == this.params.data.categoria){
            this.sitiosArray.push(this.arraySites[index]);
          }
        }
        console.log(this.sitiosArray);



        for (let index = 0; index < this.sitiosArray.length; index++) {
          if(this.sitiosArray[index]['lenguaje'] == this.db.lenguaje){
            this.sitios.push(this.sitiosArray[index]);
            this.sitiosAux.push(this.sitiosArray[index]);
          }
        }
        this.sitios = this.objectToArray(this.sitios);
        this.sitiosAux = this.objectToArray(this.sitiosAux);
        console.log(this.sitios);
        if(this.sitios < 1){
          let toast = this.toastCtrl.create({
            message: this.db.sinContenido,
            position: 'bottom',
            showCloseButton: true,
            closeButtonText: 'Menu',
          });
        
          toast.onDidDismiss(() => {
            if(this.navCtrl.getActive.name != 'HomePage'){
              this.navCtrl.setRoot('HomePage');
            }
          });
          toast.present();
        }
      },
      (err) => {
        this.params.data.loading.dismiss();
        let toast = this.toastCtrl.create({
          message: this.db.errorContenido,
          position: 'bottom',
          showCloseButton: true,
          closeButtonText: this.db.reintentar,
          dismissOnPageChange: true,
        });
      
        toast.onDidDismiss(() => {
          this.ionViewDidLoad();
        });
        toast.present();
      }
    );
  }

  getImgContent(img): SafeUrl {
    return this.sanitizer.bypassSecurityTrustUrl(img);
  }

  objectToArray(obj){
    var arr =[];
    for(let o in obj) {
      if (obj.hasOwnProperty(o)) {
        arr.push(obj[o]);
      }
    }
    return arr;
  };

  getItems(ev: any) {
    // Reset items back to all of the items
    this.sitios = [];
    this.sitios = this.sitiosAux;
    // set val to the value of the searchbar
    const val = ev.target.value;

    // if the value is an empty string don't filter the items
    if (val && val.trim() != '') {
      this.sitios = this.sitios.filter((item) => {
        return (item.nombre.toLowerCase().indexOf(val.toLowerCase()) > -1);
      })
    }
  }

}
