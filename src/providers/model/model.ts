import { Injectable } from '@angular/core';
import { AlertController, LoadingController } from 'ionic-angular';
import { HttpClient } from '@angular/common/http';
import 'rxjs/add/operator/map';


@Injectable()
export class ModelProvider {

  public lenguaje = 'mx';
  public cargando: string = 'Cargando';
  public inicio: string = 'Inicio';
  public appName: string = 'Guía turistica';
  public langChange: string = 'Cambiar idioma';
  public cancelar: string = 'Cancelar';
  public aceptar: string = 'Aceptar';
  public ir: string = 'Ir';
  public masInfo : string = 'Más info';
  public sinContenido: string = 'Al parecer aún no hay contenido en esta categoría';
  public errorContenido : string = 'Ha ocurrido un error al obtener el contenido, posiblemente no tengas conexión a internet';
  public reintentar : string = 'Reintentar';
  public seEncuentra : string = 'Se encuentra en';
  public puntuacion : string = 'Puntuación';
  public infoLugar : string = 'Información del lugar';
  public deseaSalir : string = '¿Desea salir de la aplicación?';
  /* Categorias */
  public comida: string = 'Comida';
  public entretenimiento: string = 'Entretenimiento';
  public atracciones: string = 'Atracciones';
  public transporte: string = 'Transporte';
  public hoteles: string = 'Hoteles';
  /* Bullets categoiras */
  public bulletComida: string = 'Lo mejor en comida';
  public bulletEntretenimiento: string = 'Diversión para todos';
  public bulletAtracciones: string = 'Recomendaciones para la cultura';
  public bulletTransporte: string = 'Llega a tu destino';
  public bulletHoteles: string = 'Descansa después de un día largo';




  constructor(
    private alertCtrl: AlertController,
    public loadingCtrl: LoadingController,
    public http: HttpClient
  ) {
    this.changeLang(this.lenguaje);
    
  }

  public obtenSitios(){
     return this.http
      .get('../assets/json/data.json')
      .map( resp => {
        return resp;
      });
  }

  public notification(title: string, text: string){
    this.alertCtrl.create({
      title: title,
      subTitle: text,
      buttons: ["Cerrar"]
    }).present();
  }

  public loadNotification(text: string){
    let loading = this.loadingCtrl.create({
      spinner: 'crescent',
      content: text
    });
    return loading;
  }

  public changeLang(lang : string){
    this.lenguaje = lang;
    switch (this.lenguaje) {
      case 'mx':
        // Titulos
        this.appName = 'Guía turistica';
        this.cargando = 'Cargando';
        this.langChange = 'Cambiar idioma';
        this.cancelar = 'Cancelar';
        this.aceptar = 'Aceptar';
        this.ir = 'Ir';
        this.masInfo  = 'Más info';
        this.seEncuentra = 'Se encuentra en';
        this.puntuacion = 'Puntuación';
        this.infoLugar = 'Información del lugar';
        this.deseaSalir = '¿Desea salir de la aplicación?';
        this.sinContenido = 'Al parecer aún no hay contenido en esta categoría';
        this.errorContenido  = 'Ha ocurrido un error al obtener el contenido, posiblemente no tengas conexión a internet';
        this.reintentar  = 'Reintentar';
        this.inicio = 'Inicio';
        this.comida = 'Comida';
        this.entretenimiento = 'Entretenimiento';
        this.atracciones = 'Atracciones';
        this.transporte = 'Transporte';
        this.hoteles = 'Hoteles';
        // Bullets
        this.bulletComida = 'Lo mejor en comida';
        this.bulletEntretenimiento = 'Diversión para todos';
        this.bulletAtracciones = 'Recomendaciones para la cultura';
        this.bulletTransporte = 'Llega a tu destino';
        this.bulletHoteles = 'Descansa después de un día largo';
        break;
      case 'en':
        // Titulos
        this.appName = 'Tourist guide';
        this.cargando = 'Loading';
        this.langChange = 'Change language';
        this.cancelar = 'Cancel';
        this.aceptar = 'Ok';
        this.ir = 'Go';
        this.sinContenido = 'There seems to be no content in this category yet';
        this.errorContenido  = 'An error occurred while getting the content, possibly you do not have an internet connection';
        this.reintentar  = 'Retry';
        this.masInfo  = 'More';
        this.seEncuentra = 'Is found in';
        this.puntuacion = 'Punctuation';
        this.infoLugar = 'Place information';
        this.deseaSalir = 'Do you want to exit the application?';
        this.inicio = 'Home';
        this.comida = 'Food';
        this.entretenimiento = 'Entertainment';
        this.atracciones = 'Amusement';
        this.transporte = 'Transport';
        this.hoteles = 'Hotel';
        // Bullets
        this.bulletComida = 'The best food';
        this.bulletEntretenimiento = 'Fun for all';
        this.bulletAtracciones = 'Recommendations for culture';
        this.bulletTransporte = 'Arrive at your destination';
        this.bulletHoteles = 'Rest after a long day';
        break;
      case 'fr':
        // Titulos
        this.appName = 'Guide touristique';
        this.cargando = 'Chargement';
        this.langChange = 'Changer la langue';
        this.cancelar = 'Annuler';
        this.aceptar = 'Accepter';
        this.ir = 'Aller';
        this.masInfo  = "Plus d'infos";
        this.seEncuentra = 'Se trouve dans';
        this.puntuacion = 'Ponctuation';
        this.infoLugar = "Lieu de l'information";
        this.deseaSalir = "Voulez-vous quitter l'application?",
        this.sinContenido = "Il semble qu'il n'y ait pas encore de contenu dans cette catégorie";
        this.errorContenido  = "Une erreur s'est produite lors de l'obtention du contenu, vous n'avez peut-être pas de connexion Internet";
        this.reintentar  = 'Réessayez';
        this.inicio = 'Accueil';
        this.comida = 'Nourriture';
        this.entretenimiento = 'Divertissement';
        this.atracciones = 'Attractions';
        this.transporte = 'Transport';
        this.hoteles = 'Hôtels';
        // Bullets
        this.bulletComida = 'Le meilleur de la nourriture';
        this.bulletEntretenimiento = 'Fun pour tout le monde';
        this.bulletAtracciones = 'Recommandations pour la culture';
        this.bulletTransporte = 'Arriver à votre destination';
        this.bulletHoteles = 'Reste après une longue journée';
        break;
    }
    
  }

  showRadio() {
    let alert = this.alertCtrl.create();
    alert.setTitle(this.langChange);

    let mx = false;
    let en = false;
    let fr = false;
    switch (this.lenguaje) {
      case 'mx':
        mx = true;
        break;
      case 'en':
        en = true;
        break;
      case 'fr':
        fr = true;
      break;
    }

    alert.addInput({
      type: 'radio',
      label: 'Español',
      value: 'mx',
      checked: mx
    });

    alert.addInput({
      type: 'radio',
      label: 'English',
      value: 'en',
      checked: en
    });

    alert.addInput({
      type: 'radio',
      label: 'French',
      value: 'fr',
      checked: fr
    });

    alert.addButton(this.cancelar);
    alert.addButton({
      text: this.aceptar,
      handler: data => {
        this.changeLang(data);
      }
    });
    alert.present();
  }

}
