export interface Sitio {
    key?: string;
    descripcion: string;
    estado: string;
    galeria: string;
    img: string;
    latitud: string;
    lenguaje: string;
    longitud: string;
    nombre: string;
    resumen: string;
}